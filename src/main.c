/**
 * Copyright (c) 2018 Himanshu Goel
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

#include <stdlib.h>
#include <stdio.h>

#include "fw_parser.h"

int main(int argc, char *argv[]) {

    FILE *fd = fopen(argv[1], "rb");

    fseek(fd, 0, SEEK_END);
    long data_len = ftell(fd);
    fseek(fd, 0, SEEK_SET);

    void *data = malloc(data_len);
    fread(data, 1, data_len, fd);
    fclose(fd);

    fw_info_t fwinfo;
    if(fw_parse(data, data_len, &fwinfo) == 0) {
        printf("Firmware parsing successful.\r\n");

        printf("FW Version: %x.%x.%x\r\n", fwinfo.fw_version[0], fwinfo.fw_version[1], fwinfo.fw_version[2]);
        printf("Probe Max Len: %d\r\n", fwinfo.probe_max_len);
        printf("Phy SKU: 0x%x\r\n", fwinfo.phy_sku);
        printf("Paging Size: 0x%x\r\n", fwinfo.paging_sz);
        printf("CPU Count: %d\r\n", fwinfo.num_of_cpu);
        printf("Flags: 0x%x\r\n", fwinfo.flags);
    }

    free(data);
    return 0;
}