/**
 * Copyright (c) 2018 Himanshu Goel
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */
#include "fw_parser.h"
#include "fw_defs.h"

#include <stdlib.h>
#include <string.h>

void fw_addsection(fw_info_t *fw_inf, fw_section_type_t type, void *data, size_t data_len) {
    uint32_t sec_ent = fw_inf->section_entries[type].section_ent;
    data_len -= sizeof(uint32_t);
    fw_inf->section_entries[type].sections[sec_ent].data = malloc(data_len);
    memcpy(fw_inf->section_entries[type].sections[sec_ent].data, (uint8_t*)data + sizeof(uint32_t), data_len);
    fw_inf->section_entries[type].sections[sec_ent].len = data_len;
    fw_inf->section_entries[type].sections[sec_ent].offset = *(uint32_t*)data;
    fw_inf->section_entries[type].section_ent++;
}

int fw_parse(void *data, size_t data_len, fw_info_t *fw_inf) {

    memset(fw_inf, 0, sizeof(fw_info_t));

    struct ucode_hdr *hdr = (struct ucode_hdr*)data;
    uint8_t *data_u8 = (uint8_t*)hdr->data;

    if(hdr->magic != UCODE_MAGIC)
        return -1;

    data_len -= sizeof(struct ucode_hdr);

    while(data_len > 0) {
        data_len -= sizeof(struct ucode_tlv);
        struct ucode_tlv *tlv = (struct ucode_tlv*)data_u8;

        size_t tlv_len = tlv->length;
        if(tlv_len % sizeof(uint32_t))
            tlv_len += sizeof(uint32_t) - (tlv_len % sizeof(uint32_t));

        data_len -= tlv_len;

        data_u8 += sizeof(struct ucode_tlv);
        data_u8 += tlv_len;


        switch(tlv->type) {
            case UCODE_TLV_PROBE_MAX_LEN:
                {
#ifdef DEBUG_MSG
                    printf("PROBE_MAX_LEN\r\n");
#endif
                    fw_inf->probe_max_len = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_PAGING:
                {
#ifdef DEBUG_MSG
                    printf("PAGING\r\n");
#endif
                    fw_inf->paging_sz = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_PHY_SKU:
                {
#ifdef DEBUG_MSG
                    printf("PHY SKU\r\n");
#endif
                    fw_inf->phy_sku = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_NUM_OF_CPU:
                {
#ifdef DEBUG_MSG
                    printf("NUM OF CPU\r\n");
#endif
                    fw_inf->num_of_cpu = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_N_SCAN_CHANNELS:
                {
#ifdef DEBUG_MSG
                    printf("N SCAN CHANNELS\r\n");
#endif
                    fw_inf->n_scan_channels = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_FW_VERSION:
                {
#ifdef DEBUG_MSG
                    printf("FW VERSION\r\n");
#endif
                    fw_inf->fw_version[0] = ((uint32_t*)tlv->data)[0];
                    fw_inf->fw_version[1] = ((uint32_t*)tlv->data)[1];
                    fw_inf->fw_version[2] = ((uint32_t*)tlv->data)[2];
                }
                break;
            case UCODE_TLV_PAN:
                {
#ifdef DEBUG_MSG
                    printf("PAN\r\n");
#endif
                    fw_inf->flags |= 1;
                }
                break;
            case UCODE_TLV_FLAGS:
                {
#ifdef DEBUG_MSG
                    printf("FLAGS\r\n");
#endif
                    fw_inf->flags = *(uint32_t*)tlv->data;
                }
                break;
            case UCODE_TLV_SEC_RT:
                {
#ifdef DEBUG_MSG
                    printf("SEC RT\r\n");
#endif
                    fw_addsection(fw_inf, fw_section_regular, tlv->data, tlv_len);
                }
                break;
            case UCODE_TLV_SEC_INIT:
                {
#ifdef DEBUG_MSG
                    printf("SEC INIT\r\n");
#endif
                    fw_addsection(fw_inf, fw_section_init, tlv->data, tlv_len);
                }
                break;
            case UCODE_TLV_SEC_WOWLAN:
                {
#ifdef DEBUG_MSG
                    printf("SEC WOWLAN\r\n");
#endif
                    fw_addsection(fw_inf, fw_section_wowlan, tlv->data, tlv_len);
                }
                break;
            case UCODE_TLV_SEC_RT_USNIFFER:
                {
#ifdef DEBUG_MSG
                    printf("SEC RT USNIFFER\r\n");
#endif
                    fw_addsection(fw_inf, fw_section_usniffer, tlv->data, tlv_len);
                }
                break;
            case UCODE_TLV_ENABLED_CAPABILITIES:
                {
#ifdef DEBUG_MSG
                    printf("ENABLED CAPABILITIES\r\n");
#endif
                    uint32_t idx = ((uint32_t*)tlv->data)[0];
                    uint32_t flags = ((uint32_t*)tlv->data)[1];

                    if(idx < ENABLED_CAPS_LEN)
                        fw_inf->enabled_caps[idx] |= flags;
                }
                break;
            case UCODE_TLV_API_CHANGES_SET:
                {
#ifdef DEBUG_MSG
                    printf("API CHANGES SET\r\n");
#endif
                    uint32_t idx = ((uint32_t*)tlv->data)[0];
                    uint32_t flags = ((uint32_t*)tlv->data)[1];

                    if(idx < ENABLED_API_LEN)
                        fw_inf->enabled_api[idx] |= flags;
                }
                break;
            case UCODE_TLV_DEF_CALIB:
                {
#ifdef DEBUG_MSG
                    printf("DEF CALIB\r\n");
#endif
                    struct tlv_def_calib *calib_data = (struct tlv_def_calib*)tlv->data;
                    fw_inf->section_entries[calib_data->type].flow_trigger = calib_data->flow_trigger;
                    fw_inf->section_entries[calib_data->type].event_trigger = calib_data->event_trigger;
                }
                break;
            default:
#ifdef DEBUG_MSG
                printf("Unknown: %d\r\n", tlv->type);
#endif
            break;
        }
    }

    return 0;
}